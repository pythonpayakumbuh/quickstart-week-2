const express = require('express')
const bodyParser = require('body-parser')
const mysql = require('mysql')

const app = express()

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '#Admin123',
    database: 'book_manager_library'
})

const port = 3020
app.listen(port, () => {
    console.log('This app running on port: ', port)
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}))
app.set('view enginge', 'ejs')
app.use(express.static('public'))


app.post('/', (req, res) => {
    const data = {
        name: req.body.name,
        writer: req.body.writer,
        location: req.body.location,
        category: req.body.category,
        created_at: new Date(),
        updated_at: new Date()
    }
    // prepared statement
    connection.query('INSERT INTO contact SET ?', data, (err, result) => {
        if (err) console.log(err)
        res.send(result)
    })
})


app.patch('/update/:id', (req, res) => {
    const data = {
        name: req.body.name,
        writer: req.body.writer,
        location: req.body.location,
        category: req.body.location,
        updated_at: new Date()
    }
    const userId = req.params.id

    connection.query('UPDATE contact SET ? WHERE id = ?', [data, userId], (err, result) => {
        if (err) console.log(err)
        res.send(result)
    })
})

app.delete('/:id', (req, res) => {
    const userId = req.params.id

    connection.query('DELETE FROM contact WHERE id = ?', userId, (err, result) => {
        if (err) console.log(err)
        res.send(result)

    })
})


app.get('/', (req, res) => {
    connection.query('SELECT * from contact', (err, result) => {
        if (err) console.log(err)
        res.render('index.ejs', {
            result: result
        })
    })

})

app.get('/update_view/:id', (req, res) => {
    res.render('update.ejs', {
    })
})